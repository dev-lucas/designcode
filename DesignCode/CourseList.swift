//
//  CourseList.swift
//  DesignCode
//
//  Created by Lucas Gomes on 11/05/23.
//

import SwiftUI
import SDWebImageSwiftUI

struct CourseList: View {
    @ObservedObject var store = CourseStore()
    @State var active = false
    @State var activeIndex = -1
    @State var activeView = CGSize.zero
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    @State var isScrollable = false
    
    var body: some View {
        GeometryReader { bounds in
            ZStack {
                Color.black.opacity(Double(activeView.height / 500))
                    .animation(.linear, value: active)
                    .ignoresSafeArea()
                
                ScrollView {
                    VStack(spacing: 30) {
                        Text("Courses")
                            .font(.largeTitle)
                            .bold()
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 30)
                            .padding(.top, 30)
                            .blur(radius: active ? 20 : 0)
                        
                        ForEach(store.courses.indices, id: \.self) { index in
                            GeometryReader { geometry in
                                CourseView(
                                    show: $store.courses[index].show,
                                    course: store.courses[index],
                                    active: $active,
                                    index: index,
                                    activeIndex: $activeIndex,
                                    activeView: $activeView,
                                    bounds: bounds,
                                    isScrollable: $isScrollable
                                )
                                .offset(y: store.courses[index].show ? -geometry.frame(in: .global).minY : 0)//Joga para o topo
                                .opacity(active && activeIndex != index ? 0 : 1)//Vai nos cards restantes que nao estao selecionados
                                .scaleEffect(active && activeIndex != index ? 0.5 : 1)//Vai nos cards restantes que nao estao selecionados
                                .offset(x: active && activeIndex != index ?  bounds.size.width : 0)//Vai nos cards restantes que nao estao selecionados
                            }
                            .frame(height: horizontalSizeClass == .regular ? 80 : 280)
                            .frame(maxWidth: store.courses[index].show ? 712 : getCardWidth(bounds))//MARK: 712 tamanho maximo recomendado para ipad
                            .zIndex(store.courses[index].show ? 1 : 0)
                        }
                    }
                    .frame(width: bounds.size.width)
                    .animation(.spring(response: 0.5, dampingFraction: 0.6), value: active)
                }
                .scrollDisabled(active ? true : false)
                .statusBarHidden(active ? true : false)
                .animation(.linear, value: active)
            }
        }
    }
}

func getCardWidth(_ bounds: GeometryProxy) -> CGFloat {
    if bounds.size.width > 712 {
        return 712
    }
    return bounds.size.width - 60
}

func getCardCornerRadius(_ bounds: GeometryProxy) -> CGFloat {
    // (Se a tela nao for pequena) && (Se a tela nao tem noch)
    if bounds.size.width < 712 && bounds.safeAreaInsets.top < 44 {
        return 0
    }
    return 30
}

struct CourseList_Previews: PreviewProvider {
    static var previews: some View {
        CourseList()
    }
}

struct CourseView: View {
    @Binding var show: Bool
    var course: Course
    @Binding var active: Bool
    var index: Int
    @Binding var activeIndex: Int
    @Binding var activeView: CGSize
    var bounds: GeometryProxy
    @Binding var isScrollable: Bool
    
    var body: some View {
        ZStack(alignment: .top) {
            VStack(alignment: .leading, spacing: 30.0) {
                Text("Take your SwiftUI app to the App Store with advanced techniques like API data, packages and CMS.")
                
                Text("About this course")
                    .font(.title).bold()
                
                Text("This course is unlike any other. We care about design and want to make sure that you get better at it in the process. It was written for designers and developers who are passionate about collaborating and building real apps for iOS and macOS. While it's not one codebase for all apps, you learn once and can apply the techniques and controls to all platforms with incredible quality, consistency and performance. It's beginner-friendly, but it's also packed with design tricks and efficient workflows for building great user interfaces and interactions.")
                
                Text("Minimal coding experience required, such as in HTML and CSS. Please note that Xcode 11 and Catalina are essential. Once you get everything installed, it'll get a lot friendlier! I added a bunch of troubleshoots at the end of this page to help you navigate the issues you might encounter.")
            }
//            .animation(nil)
            .padding(30)
            .frame(maxWidth: CGFloat(show ? .infinity : screen.width - 60), maxHeight: CGFloat(show ? .infinity : 280.0), alignment: .top)
            .offset(y: show ? 460 : 0)
            .background(Color("background1"))
            .clipShape(RoundedRectangle(cornerRadius: show ? getCardCornerRadius(bounds) : 30, style: .continuous))
            .shadow(color: Color.black.opacity(0.2), radius: 20, x: 0, y: 20)
            .opacity(show ? 1 : 0)
            
            VStack {
                HStack(alignment: .top) {
                    VStack(alignment: .leading, spacing: 8.0) {
                        Text(course.title)
                            .font(.system(size: 24, weight: .bold))
                            .foregroundColor(.white)
                        Text(course.subtitle)
                            .foregroundColor(Color.white.opacity(0.7))
                    }
                    Spacer()
                    ZStack {
                        Image(uiImage: course.logo)
                            .opacity(show ? 0 : 1)
                        
                        VStack {
                            Image(systemName: "xmark")
                                .font(.system(size: 16, weight: .medium))
                                .foregroundColor(.white)
                        }
                        .frame(width: 36, height: 36)
                        .background(Color.black)
                        .clipShape(Circle())
                        .opacity(show ? 1 : 0)
                        .offset(x: 2, y: -2)
                    }
                }
                Spacer()
                WebImage(url: course.image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(maxWidth: .infinity)
                    .frame(height: 140, alignment: .top)
            }
            .padding(show ? 30 : 20)
            .padding(.top, CGFloat(show ? 30 : 0))
            //        .frame(width: show ? screen.width : screen.width - 60, height: show ? screen.height : 280)
            .frame(maxWidth: CGFloat(show ? .infinity : screen.width - 60), maxHeight: CGFloat(show ? 460 : 280))
            .background(Color(course.color))
            .clipShape(RoundedRectangle(cornerRadius: show ? getCardCornerRadius(bounds) : 30, style: .continuous))
            .shadow(color: Color(course.color).opacity(0.3), radius: 20, x: 0, y: 20)
            .gesture(
                show ?
                DragGesture()
                    .onChanged { value in
                        guard value.translation.height < 300 else { return }
                        guard value.translation.height > 0 else { return }
                        
//                        activeGesture = true
                        activeView = value.translation
                    }
                    .onEnded { value in
                        if activeView.height > 50 {
                            show = false
                            active = false
                            activeIndex = -1
                            isScrollable = false
//                            activeGesture = false
                        }
                        activeView = .zero
                    }
                : nil
            )
            .onTapGesture {
                show.toggle()
                active.toggle()
                if show {
                    activeIndex = index
                } else {
                    activeIndex = -1
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                    isScrollable = true
                }
            }
            
            if isScrollable {//MARK: Mostra o clone da tela de detalhes
                CourseDetail(course: course, show: $show, active: $active, activeIndex: $activeIndex, isScrollable: $isScrollable, bounds: bounds)
                    .background(.white)
                    .animation(nil, value: show)
            }
        }
        .frame(height: show ? bounds.size.height + bounds.safeAreaInsets.top + bounds.safeAreaInsets.bottom : 280)
        .scaleEffect(1 - (activeView.height.rounded(.towardZero) / 1000))//.rounded(.towardZero) evita o congelamento da tela
        .rotation3DEffect(.degrees(activeView.height.rounded(.towardZero) / 10), axis: (x: 0, y: 10, z: 0))
        .hueRotation(.degrees(activeView.height.rounded(.towardZero) / 2))//Muda a cor
        .animation(.spring(response: 0.5, dampingFraction: 0.6), value: show)
        .animation(.spring(response: 0.5, dampingFraction: 0.6), value: activeView)
        .gesture(
            show ?
            DragGesture()
                .onChanged { value in
                    guard value.translation.height < 300 else { return }
                    guard value.translation.height > 0 else { return }
                    activeView = value.translation
                }
                .onEnded { value in
                    if activeView.height > 50 {
                        show = false
                        active = false
                        activeIndex = -1
                    }
                    activeView = .zero
                }
            : nil
        )
        .ignoresSafeArea()
    }
}

struct Course: Identifiable {
    var id = UUID()
    var title: String
    var subtitle: String
    var image: URL
    var logo: UIImage
    var color: UIColor
    var show: Bool
}

var courseData: [Course] = [
    Course(title: "Prototype Designs in SwiftUI", subtitle: "18 Sections", image: URL(string: "https://images.ctfassets.net/y86v4c4peepp/6U3gEa33eLdcldW72b27IR/5555dfd6329e4c6456d2a09d52926ff4/Certificate_2x.png")!, logo: #imageLiteral(resourceName: "Logo1"), color: #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1), show: false),
    Course(title: "SwiftUI Advanced", subtitle: "20 Sections", image: URL(string: "https://images.ctfassets.net/y86v4c4peepp/2DfMm3fBMyu5dsCQJDIn4m/2a3411f2e87e2a3e30d0298e092657ef/Card3_2x.png")!, logo: #imageLiteral(resourceName: "Logo1"), color: #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1), show: false),
    Course(title: "UI Design for Developers", subtitle: "20 Sections", image: URL(string: "https://images.ctfassets.net/y86v4c4peepp/40xK84ZpeEeqjl0xT9cTIz/8c2b293c55b6fc46e7347c7bb3ba74a7/Card4_2x.png")!, logo: #imageLiteral(resourceName: "Logo3"), color: #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1), show: false)
]
