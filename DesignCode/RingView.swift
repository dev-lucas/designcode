//
//  RingView.swift
//  DesignCode
//
//  Created by Lucas Gomes on 10/05/23.
//

import SwiftUI

struct RingView: View {
    var color1 = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    var color2 = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
    var width: CGFloat = 200
    var height: CGFloat = 200
    var percent: CGFloat = 88
    var animation: Animation = .timingCurve(0.71, 0.27, 0.21, 0.71, duration: 1).delay(0.2)
    @Binding var show: Bool
    
    var body: some View {
        let multiplier = width / 44
        let progress = 1 - (percent / 100) //O (-1) inverte a ordem
        
        func parsePercent() -> Int {
            if percent > 100 {
                return 100
            }
            
            if percent < 0 {
                return 0
            }
            
            return Int(percent)
        }
        
        return ZStack {
            Circle()
                .stroke(.black.opacity(0.1), style: StrokeStyle(lineWidth: 5 * multiplier))
                .frame(width: width, height: height)
                
            
            Circle()
                .trim(from: show ? progress : 1, to: 1)
                .stroke(
                    .linearGradient(colors: [Color(color1), Color(color2)], startPoint: .topTrailing, endPoint: .bottomLeading),
                    style: StrokeStyle(lineWidth: 5 * multiplier, lineCap: .round, lineJoin: .round, miterLimit: .infinity, dash: [20, 0], dashPhase: 0)
                )
                .animation(animation, value: show)
                .rotationEffect(.degrees(90))
                .rotation3DEffect(.degrees(180), axis: (x: 1, y: 0, z: 0))
                .frame(width: width, height: height)
                .shadow(color: Color(color2).opacity(0.1), radius: 3 * multiplier, x: 0, y: 3 * multiplier)
            
            Text("\(parsePercent())%")
                .font(.system(size: 14 * multiplier))
                .fontWeight(.bold)
        }
    }
}

struct RingView_Previews: PreviewProvider {
    static var previews: some View {
        RingView(show: .constant(true))
    }
}
