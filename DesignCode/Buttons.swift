//
//  Buttons.swift
//  DesignCode
//
//  Created by Lucas Gomes on 12/05/23.
//

import SwiftUI

func haptic(type: UINotificationFeedbackGenerator.FeedbackType) {
    // Vibracao no celular (.error, .success, .warning)
    UINotificationFeedbackGenerator().notificationOccurred(type)
    print("haptic")
}

func impact(style: UIImpactFeedbackGenerator.FeedbackStyle) {
    // Vibracao no celular (.light, .medium, .heavy, .soft, .rigid)
    UIImpactFeedbackGenerator(style: .heavy).impactOccurred()
    print("impact")
}

struct Buttons: View {
    
    var body: some View {
//        let color1 = #colorLiteral(red: 0.7608050108, green: 0.8164883852, blue: 0.9259157777, alpha: 1)
//        let color2 = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //White
        let color3 = #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1)
//        let color4 = #colorLiteral(red: 0.9019607843, green: 0.9294117647, blue: 0.9882352941, alpha: 1)
//        let color5 = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
        
        VStack(spacing: 50.0) {
            RectangleButton()
            
            CircleButton()
            
            PayButton()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color(color3))
  
    }
}

struct Buttons_Previews: PreviewProvider {
    static var previews: some View {
        Buttons()
    }
}

struct RectangleButton: View {
    @State var tap = false
    @State var press = false
    
    var body: some View {
        let color1 = #colorLiteral(red: 0.7608050108, green: 0.8164883852, blue: 0.9259157777, alpha: 1)
        let color2 = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //White
        let color4 = #colorLiteral(red: 0.9019607843, green: 0.9294117647, blue: 0.9882352941, alpha: 1)
        let color5 = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
        
        ZStack {
            Text("Button")
                .font(.system(size: 20, weight: .semibold, design: .rounded))
                .foregroundColor(.black)
                .frame(width: 200, height: 60)
                .background(
                    ZStack {
                        Color(press ? color2 : color1)
                            .ignoresSafeArea()
                        
                        RoundedRectangle(cornerRadius: 16, style: .continuous)
                            .foregroundColor(Color(press ? color1 : color2))
                            .blur(radius: 4)
                            .offset(x: -8, y: -8)
                        
                        RoundedRectangle(cornerRadius: 16, style: .continuous)
                            .fill(
                                .linearGradient(colors: [Color(color4), Color.white], startPoint: .topLeading, endPoint: .bottomTrailing)
                            )
                            .padding(2)
                            .blur(radius: 2)
                    }
                )
                .clipShape(RoundedRectangle(cornerRadius: 16, style: .continuous))
                .overlay(
                    HStack {
                        Image(systemName: "person.crop.circle")
                            .font(.system(size: 24, weight: .light))
                            .foregroundColor(Color(press ? color5 : color2))
                            .frame(width: press ? 64 : 54, height: press ? 4 : 50)
                            .background(Color(color5))
                            .clipShape(RoundedRectangle(cornerRadius: 16, style: .continuous))
                            .shadow(color: Color(color5).opacity(0.3), radius: 10, x: 10, y: 10)
                            .offset(x: press ? 70 : -10, y: press ? 16 : 0)
                        Spacer()
                    }
                )
                .shadow(color: Color(press ? color2 : color1), radius: 20, x: 20, y: 20)
                .shadow(color: Color(press ? color1 : color2), radius: 20, x: -20, y: -20)
                .scaleEffect(tap ? 1.2 : 1)
                .gesture(
                    LongPressGesture(minimumDuration: 0.5, maximumDistance: 10)
                        .onChanged { value in
                            tap = true
                            impact(style: .heavy)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                tap = false
                            }
                        }
                        .onEnded { value in
                            press.toggle()
                            haptic(type: .success)
                        }
                )
        }
        .animation(.spring(response: 0.5, dampingFraction: 0.5), value: tap)
        .animation(.spring(response: 0.4, dampingFraction: 0.8), value: press)
    }
}

struct CircleButton: View {
    @State var tap = false
    @State var press = false
    
    var body: some View {
        let color1 = #colorLiteral(red: 0.7608050108, green: 0.8164883852, blue: 0.9259157777, alpha: 1)
        let color2 = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //White
        let color3 = #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1)
        
        ZStack {
            Image(systemName: "sun.max")
                .font(.system(size: 44, weight: .light))
                .foregroundColor(.black)
                .offset(x: press ? -90 : 0, y: press ? -90 : 0)
                .rotation3DEffect(.degrees(press ? 20 : 0), axis: (x: 10, y: -10, z: 0))
            
            Image(systemName: "moon")
                .font(.system(size: 44, weight: .light))
                .foregroundColor(.black)
                .offset(x: press ? 0 : 90, y: press ? 0 : 90)
                .rotation3DEffect(.degrees(press ? 0 : 20), axis: (x: 10, y: -10, z: 0))
        }
        .frame(width: 100, height: 100)
        .animation(.spring(response: 0.5, dampingFraction: 0.4), value: press)
        .background(
            ZStack {
                LinearGradient(colors: [Color(press ? color3 : color2), Color(press ? color2 : color3)], startPoint: .topLeading, endPoint: .bottomTrailing)
                
                Circle()
                    .stroke(Color.black.opacity(0.001), lineWidth: 10)
                    .shadow(color: Color(press ? color2 : color1), radius: 3, x: -5, y: -5)
                    .scaleEffect(1.16)
                
                Circle()
                    .stroke(Color.black.opacity(0.001), lineWidth: 10)
                    .shadow(color: Color(press ? color1 : color2), radius: 3, x: 3, y: 3)
                    .scaleEffect(1.16)
            }
            .animation(.spring(response: 0.5, dampingFraction: 0.4), value: press)
        )
        .clipShape(Circle())
        .shadow(color: Color(press ? color1 : color2), radius: 20, x: -20, y: -20)
        .shadow(color: Color(press ? color2 : color1), radius: 20, x: 20, y: 20)
        .scaleEffect(tap ? 1.2 : 1)
        .gesture(
            LongPressGesture()
                .onChanged { value in
                    tap = true
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        tap = false
                    }
                }
                .onEnded { value in
                    press.toggle()
                }
        )
        .animation(.spring(response: 0.5, dampingFraction: 0.5), value: tap)
    }
}

struct PayButton: View {
    @GestureState var tap = false
    @State var press = false
    
    var body: some View {
        let color1 = #colorLiteral(red: 0.7608050108, green: 0.8164883852, blue: 0.9259157777, alpha: 1)
        let color2 = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //White
        let color3 = #colorLiteral(red: 0.8980392157, green: 0.9333333333, blue: 1, alpha: 1)
        let color5 = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
        let color6 = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        
        ZStack {
            Image("fingerprint")
                .opacity(press ? 0 : 1)
                .scaleEffect(press ? 0 : 1)
            
            Image("fingerprint-2")
                .clipShape(Rectangle().offset(y: tap ? 0 : 50))
                .opacity(press ? 0 : 1)
                .scaleEffect(press ? 0 : 1)
            
            Image(systemName: "checkmark.circle.fill")
                .font(.system(size: 44, weight: .light))
                .foregroundColor(Color(color5))
                .opacity(press ? 1 : 0)
                .scaleEffect(press ? 1 : 0)
        }
        .frame(width: 120, height: 120)
        .animation(.spring(response: 0.5, dampingFraction: 0.5), value: press)
        .animation(.easeOut, value: tap)
        .background(
            ZStack {
                LinearGradient(colors: [Color(press ? color3 : color2), Color(press ? color2 : color3)], startPoint: .topLeading, endPoint: .bottomTrailing)
                
                Circle()
                    .stroke(Color.black.opacity(0.001), lineWidth: 10)
                    .shadow(color: Color(press ? color2 : color1), radius: 3, x: -5, y: -5)
                    .scaleEffect(1.16)
                
                Circle()
                    .stroke(Color.black.opacity(0.001), lineWidth: 10)
                    .shadow(color: Color(press ? color1 : color2), radius: 3, x: 3, y: 3)
                    .scaleEffect(1.16)
            }
            .animation(.spring(response: 0.5, dampingFraction: 0.4), value: press)
        )
        .clipShape(Circle())
        .overlay(
            Circle()
                .trim(from: tap ? 0.001 : 1, to: 1)
                .stroke(.linearGradient(colors: [Color(color5), Color(color6)], startPoint: .topLeading, endPoint: .bottomTrailing), style: StrokeStyle(lineWidth: 5, lineCap: .round))
                .frame(width: 88, height: 88)
                .rotationEffect(.degrees(90))
                .rotation3DEffect(.degrees(180), axis: (x: 1, y: 0, z: 0))
                .shadow(color: Color(color5).opacity(0.3), radius: 5, x: 3, y: 3)
                .animation(.easeInOut, value: tap)
        )
        .shadow(color: Color(press ? color1 : color2), radius: 20, x: -20, y: -20)
        .shadow(color: Color(press ? color2 : color1), radius: 20, x: 20, y: 20)
        .scaleEffect(tap ? 1.2 : 1)
        .gesture(
            LongPressGesture()
                .updating($tap) { currentState, gestureState, transaction in
                    gestureState = currentState
                }
                .onEnded { value in
                    press.toggle()
                    haptic(type: .success)
                }
        )
        .animation(.spring(response: 0.5, dampingFraction: 0.4), value: tap)
    }
}
