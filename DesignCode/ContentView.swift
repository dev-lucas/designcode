//
//  ContentView.swift
//  DesignCode
//
//  Created by Lucas Gomes on 09/05/23.
//

import SwiftUI

struct ContentView: View {
    @State var show = false
    @State var viewState = CGSize.zero
    @State var showCard = false
    @State var bottomState = CGSize.zero
    @State var showFull = false
    
    /* Outra forma de animar uma view especifica
     .animation(.easeInOut(duration: 0.5), value: show)
     */
    
    var body: some View {
        ZStack {//Necessario para posicionar views atras ou na frente umas das outras
            titleView
                .blur(radius: show ? 20 : 0)
                .opacity(showCard ? 0.4 : 1)
                .offset(y: showCard ? -200 : 0)
                .animation(.default, value: show)//Outra forma de animar uma view especifica
                .animation(
                    .default
                        .delay(0.1),
//                        .speed(2)
//                        .repeatCount(3, autoreverses: false)
                    value: showCard
                )//Outra forma de animar uma view especifica
            
            backCardView
                .frame(maxWidth: showCard ? 300 : 340)
                .frame(height: 220)
                .background(show ? Color("card3") : Color("card4"))
                .cornerRadius(20)
                .shadow(radius: 20)
                .offset(x: 0, y: show ? -400 : -40)
                .offset(x: viewState.width, y: viewState.height)//Adicionar os valores do viewState concatenando os valores ja existentes acima
                .offset(y: showCard ? -180 : 0)
                .scaleEffect(showCard ? 1 : 0.9)
                .rotationEffect(.degrees(show ? 0 : 10))
                .rotationEffect(.degrees(showCard ? -10 : 0))//-10 para cancelar a rotacao acima
                .rotation3DEffect(.degrees(showCard ? 0 : 10), axis: (x: 10, y: 0, z: 0))
                .blendMode(.hardLight)
                .animation(.easeInOut(duration: 0.5), value: show)//
                .animation(.easeInOut(duration: 0.5), value: showCard)//onTapGesture do 'cardView' (quando clica no card)
                .animation(.easeInOut(duration: 0.4), value: viewState)//DragGesture do 'cardView' (quando mexer no card)
            
            backCardView
                .frame(maxWidth: 340)
                .frame(height: 220)
                .background(show ? Color("card4") : Color("card3"))
                .cornerRadius(20)
                .shadow(radius: 20)
                .offset(x: 0, y: show ? -200 : -20)
                .offset(x: viewState.width, y: viewState.height)//Adicionar os valores do viewState concatenando os valores ja existentes acima
                .offset(y: showCard ? -140 : 0)
                .scaleEffect(showCard ? 1 : 0.95)
                .rotationEffect(.degrees(show ? 0 : 5))
                .rotationEffect(.degrees(showCard ? -5 : 0))//-5 para cancelar a rotacao acima
                .rotation3DEffect(.degrees(showCard ? 0 : 5), axis: (x: 10, y: 0, z: 0))
                .blendMode(.hardLight)
                .animation(.easeInOut(duration: 0.3), value: show)//
                .animation(.easeInOut(duration: 0.3), value: showCard)//onTapGesture do 'cardView' (quando clica no card)
                .animation(.easeInOut(duration: 0.2), value: viewState)//DragGesture do 'cardView' (quando mexer no card)
            
            cardView
                .frame(maxWidth: showCard ? 375 : 340)
                .frame(height: 220)
                .background(Color.black)
//                .cornerRadius(20)
                .clipShape(RoundedRectangle(cornerRadius: showCard ? 30 : 20, style: .continuous))
                .shadow(radius: 20)
                .offset(x: viewState.width, y: viewState.height)
                .offset(y: showCard ? -100 : 0)
                .blendMode(.hardLight)
                .animation(.spring(response: 0.3, dampingFraction: 0.6), value: show)//Outra forma de animar uma view especifica
                .animation(.spring(response: 0.3, dampingFraction: 0.6), value: showCard)//Outra forma de animar uma view especifica
                .onTapGesture {
                    showCard.toggle()
                }
                .gesture(
                    DragGesture()
                        .onChanged { value in
                            show = true
                            viewState = value.translation
                        }
                        .onEnded { value in
                            show = false
                            viewState = .zero
                        }
                )
            
//            Text("Value: \(bottomState.height)").offset(y: -300)
//            Text("Value: \(String(show))").offset(y: -300)
            
//            RingView(color1: .red, color2: .blue, width: 88, height: 88, percent: 78, show: $showCard)
//                .animation(.easeInOut.delay(0.3), value: showCard)
            
            GeometryReader { bounds in
                bottomCardView
                    .offset(x: 0, y: showCard ? bounds.size.height / 2 : bounds.size.height + bounds.safeAreaInsets.top + bounds.safeAreaInsets.bottom)//MARK: Esconder a view de forma dinamica
                    .offset(y: bottomState.height)
                    .blur(radius: show ? 20 : 0)
                    .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8), value: showCard)//Outra forma de animar uma view especifica
                    .animation(.spring(response: 0.3, dampingFraction: 0.5), value: bottomState)//Outra forma de animar uma view especifica
                //MARK: - Melhor forma para Modal com DragGesture
                    .gesture(
                        DragGesture()
                            .onChanged { value in
                                bottomState = value.translation
                                
                                //Atribui o valor para modal maior
                                if showFull {
                                    bottomState.height += -300
                                }
                                
                                //Fixa a altura maxima para o modal
                                if bottomState.height < -300 {
                                    bottomState.height = -300
                                }
                            }
                            .onEnded { value in
                                //Esconde o modal
                                if bottomState.height > 50 {
                                    showCard = false
                                }
                                
                                //Ativa o modal maior
                                if (bottomState.height < -100 && !showFull) || (bottomState.height < -250 && showFull) {
                                    bottomState.height = -300
                                    showFull = true
                                } else {
                                    //Retorna para a posicao normal
                                    bottomState = .zero
                                    showFull = false
                                }
                            }
                    )
            }
//            .ignoresSafeArea()
            //MARK: - End (Melhor forma para Modal com DragGesture)
        }
    }
    
    var cardView: some View {
        VStack {//MARK: Main Card
            HStack {
                VStack(alignment: .leading) {
                    Text("UI Design")
                        .font(.title)
                        .fontWeight(.semibold)
                        .foregroundColor(.white)
                    
                    Text("Certificate")
                        .foregroundColor(Color("accent"))
                }
                Spacer()
                Image("Logo1")
            }
            .padding(.horizontal, 20)
            .padding(.top, 20)
            Spacer()
            Image("Card1")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 300, height: 110, alignment: .top)
        }
    }

    var backCardView: some View {
        VStack {//MARK: Second Back Card
            Spacer()
        }
    }

    var titleView: some View {
        VStack {
            HStack {
                Text("Certificates")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                Spacer()
            }
            .padding()
            Image("Background1")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(maxWidth: 375)
            
            Spacer()
        }
    }
    
    
    //MARK: - a
    var bottomCardView: some View {
        let color1 = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        let color2 = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        
        return VStack(spacing: 20) {
            Rectangle()
                .frame(width: 40, height: 6)
                .cornerRadius(3)
                .opacity(0.1)
            
            Text("This certificate is proof that Meng To has achieved the UI Design course with approval from a Design+Code instructor.")
                .multilineTextAlignment(.center)
                .font(.subheadline)
                .lineSpacing(4)
            
            HStack(spacing: 20.0) {
                RingView(color1: color1, color2: color2, width: 88, height: 88, percent: 78, animation: .easeInOut.delay(0.3), show: $showCard)
                
                VStack(alignment: .leading, spacing: 8.0) {
                    Text("SwiftUI")
                        .fontWeight(.bold)
                    
                    Text("12 of 12 section completed\n10 hours spent so far")// \n10 pula a linha
                        .font(.footnote)
                        .foregroundColor(.gray)
                        .lineSpacing(4)
                }
                .padding(20)
                .background(Color("background3"))
                .cornerRadius(20)
                .shadow(color: .black.opacity(0.2), radius: 20, x: 0, y: 10)
            }
            //RingView(color1: color1, color2: color2, width: 88, height: 88, percent: 78, show: $showCard)
            
            Spacer()
        }
        .padding(.top, 8)
        .padding(.horizontal, 20)
        .frame(maxWidth: 712) //MARK: 712 é o tamanho maximo recomendado para ipads
        .background(BlurView(style: .systemThinMaterial))//background customizado
        .cornerRadius(30)
        .shadow(radius: 20)
        .frame(maxWidth: .infinity)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            //.previewLayout(.fixed(width: 320, height: 667)) //Tamanho do antigo iphone se
    }
}
