//
//  KeyboardObserver.swift
//  DesignCode
//
//  Created by Lucas Gomes on 14/05/23.
//

import SwiftUI
import Combine

class KeyboardObserver: ObservableObject {
    private var cancellables = Set<AnyCancellable>()
    
    @Published var isKeyboardActive = false
    
    init() {
        NotificationCenter.default.publisher(for: UIResponder.keyboardWillShowNotification)
            .sink { [weak self] _ in
                self?.isKeyboardActive = true
            }
            .store(in: &cancellables)
        
        NotificationCenter.default.publisher(for: UIResponder.keyboardWillHideNotification)
            .sink { [weak self] _ in
                self?.isKeyboardActive = false
            }
            .store(in: &cancellables)
    }
}
