//
//  LoginView.swift
//  DesignCode
//
//  Created by Lucas Gomes on 13/05/23.
//

import SwiftUI
import FirebaseAuth

struct LoginView: View {
    @State var email = ""
    @State var password = ""
    //    @State var isFocused = false
    @ObservedObject var keyboardObserver = KeyboardObserver()
    @State var showAlert = false
    @State var alertMessage = "Something went wrong."
    @State var isLoading = false
    @State var isSuccessful = false
    @EnvironmentObject var user: UserStore
    
    func login() {
        hideKeyBoard()
        isLoading = true
        
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            isLoading = false
            if error != nil {
                alertMessage = error?.localizedDescription ?? ""
                showAlert = true
            } else {
                user.isLogged = true
                showAlert = false
                isSuccessful = true
                UserDefaults.standard.set(true, forKey: "isLogged")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    isSuccessful = false
                    password = ""
                    email = ""
                    user.showLogin = false
                }
            }
        }
    }
    
    func hideKeyBoard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    var body: some View {
        let color1 = #colorLiteral(red: 0.6549019608, green: 0.7137254902, blue: 0.862745098, alpha: 1)
        let color2 = #colorLiteral(red: 0, green: 0.7529411765, blue: 1, alpha: 1)
        
        ZStack {
            Color.black
                .ignoresSafeArea()
            
            ZStack(alignment: .top) {
                Color("background2")
                    .clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
                //                    .frame(minHeight: 500)
                    .ignoresSafeArea(edges: .bottom)
                
                CoverView()
                
                VStack {
                    HStack {
                        Image(systemName: "person.crop.circle.fill")
                            .foregroundColor(Color(color1))
                            .frame(width: 44, height: 44)
                            .background(.white)
                            .clipShape(RoundedRectangle(cornerRadius: 16, style: .continuous))
                            .shadow(color: .black.opacity(0.15), radius: 5, x: 0, y: 5)
                            .padding(.leading)
                        
                        TextField("Your Email".uppercased(), text: $email)
                            .keyboardType(.emailAddress)
                            .font(.subheadline)
                        //                    .textFieldStyle(.roundedBorder)
                            .padding(.leading)
                            .frame(height: 44)
                        //                            .onTapGesture {
                        //                                isFocused = true
                        //                            }
                    }
                    
                    Divider()
                        .padding(.leading, 80)
                    
                    HStack {
                        Image(systemName: "lock.fill")
                            .foregroundColor(Color(color1))
                            .frame(width: 44, height: 44)
                            .background(.white)
                            .clipShape(RoundedRectangle(cornerRadius: 16, style: .continuous))
                            .shadow(color: .black.opacity(0.15), radius: 5, x: 0, y: 5)
                            .padding(.leading)
                        
                        SecureField("You Password".uppercased(), text: $password)
                            .keyboardType(.default)
                            .font(.subheadline)
                            .padding(.leading)
                            .frame(height: 44)
                        //                            .onTapGesture {
                        //                                isFocused = true
                        //                            }
                    }
                }
                .frame(height: 136)
                .frame(maxWidth: 712)
                .background(BlurView())
                .clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
                .shadow(color: .black.opacity(0.15), radius: 20, x: 0, y: 20)
                .padding(.horizontal)
                .offset(y: 460)
                
                HStack {
                    Text("Forgot password?")
                    
                    Spacer()
                    
                    Button {
                        login()
                        
                        //                        isFocused = false
                    } label: {
                        Text("Log in")
                            .foregroundColor(.black)
                    }
                    .padding(12)
                    .padding(.horizontal, 30)
                    .background(Color(color2))
                    .clipShape(RoundedRectangle(cornerRadius: 20, style: .continuous))
                    .shadow(color: Color(color2).opacity(0.3), radius: 20, x: 0, y: 20)
                    .alert(isPresented: $showAlert) {
                        Alert(title: Text("Error"), message: Text(alertMessage), dismissButton: .default(Text("OK")))
                    }
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom)
                .padding()
                
            }
            .offset(y: keyboardObserver.isKeyboardActive ? -300 : 0)
            //            .animation(.easeInOut, value: isFocused)
            .animation(.easeInOut, value: keyboardObserver.isKeyboardActive)
            .onTapGesture {
                //                isFocused = false
                hideKeyBoard()
            }
            
            if isLoading {
                LoadingView()
            }
            
            if isSuccessful {
                SuccessView()
            }
        }
        .ignoresSafeArea(.keyboard)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

struct CoverView: View {
    @State var show = false
    @State var viewState = CGSize.zero
    @State var isDragging = false
    @State var rotationAngleX: Double = 0.0
    @State var rotationAngleY: Double = 0.0
    
    var body: some View {
        let color1 = #colorLiteral(red: 0.4117647059, green: 0.4705882353, blue: 0.9725490196, alpha: 1)
        
        VStack {
            //MARK: - Ajusta o tamanho da fonte de acordo com a largura da tela
            GeometryReader { geometry in
                Text("Learn design & code.\nFrom scratch.")
                    .font(.system(size: geometry.size.width/10, weight: .bold))
                    .foregroundColor(.black).opacity(0.1)
                    .blur(radius: 1)
                    .scaleEffect(isDragging ? 0.9 : 1)
                //                    .opacity(isDragging ? 1 : 0)
                //                        .offset(x: viewState.width / -50, y: viewState.height / -50)
                
                Text("Learn design & code.\nFrom scratch.")
                    .font(.system(size: geometry.size.width/10, weight: .bold))
                    .foregroundColor(.white)
                    .offset(x: viewState.width / 15, y: viewState.height / 15)
                //                        .shadow(color: .white.opacity(0.4), radius: 9, x: 1, y: 0)
                
            }
            .frame(maxWidth: 375, maxHeight: 100)
            .padding(.horizontal, 16)
            
            //MARK: -
            
            ZStack {
                Text("80 hours of courses for SwiftUI, React and design tools.")
                    .font(.subheadline)
                    .foregroundColor(.black).opacity(0.1)
                    .blur(radius: 1)
                    .scaleEffect(isDragging ? 0.9 : 1)
                //                    .opacity(isDragging ? 1 : 0)
                
                Text("80 hours of courses for SwiftUI, React and design tools.")
                    .font(.subheadline)
                    .offset(x: viewState.width / 20, y: viewState.height / 20)
            }
            .frame(width: 250)
            
            //                Text("X: \(viewState.width)")
            //                    .font(.subheadline)
            //                    .frame(width: 250)
            //
            //
            //                Text("Y: \(viewState.height)")
            //                    .font(.subheadline)
            //                    .frame(width: 250)
            
            Spacer()
        }
        .multilineTextAlignment(.center)
        .padding(.top, 100)
        .frame(height: 477)
        .frame(maxWidth: .infinity)
        //MARK: - Fundo com animacao
        .background(
            ZStack {
                Image("Blob")
                    .offset(x: -150, y: -200)
                    .rotationEffect(.degrees(show ? 360+90 : 90))
                    .blendMode(.plusDarker)
                    .onAppear {
                        show = true
                    }
                    .animation(.linear(duration: 50).repeatForever(autoreverses: false), value: show)
                
                Image("Blob")
                    .offset(x: -200, y: -250)
                    .rotationEffect(.degrees(show ? 360 : 0), anchor: .leading)
                    .blendMode(.overlay)
                    .animation(.linear(duration: 90).repeatForever(autoreverses: false), value: show)
            }
        )
        //MARK: -
        .background(
            Image("Card3")
                .offset(x: viewState.width / 25, y: viewState.height / 25),
            alignment: .bottom)
        .background(Color(color1))
        .clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
        .scaleEffect(isDragging ? 0.9 : 1)
        .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8), value: isDragging)
        //            .rotation3DEffect(.degrees(5), axis: (x: viewState.width, y: viewState.height, z: 0))
        .rotation3DEffect(
            Angle(degrees: rotationAngleX),
            axis: (x: 1.0, y: 0.0, z: 0.0)
        )
        .rotation3DEffect(
            Angle(degrees: rotationAngleY),
            axis: (x: 0.0, y: 1.0, z: 0.0)
        )
        .gesture(
            DragGesture()
                .onChanged { value in
                    let vector = CGVector(dx: value.translation.width, dy: value.translation.height)
                    rotationAngleX = Double(-vector.dy / 30)// 100 -> less angle
                    rotationAngleY = Double(vector.dx / 30)// (-vector.dx) & (vector.dy) to invert rotation
                    viewState = value.translation
                    isDragging = true
                }
                .onEnded { value in
                    viewState = .zero
                    isDragging = false
                    rotationAngleX = 0.0
                    rotationAngleY = 0.0
                }
        )
    }
}
