//
//  UserStore.swift
//  DesignCode
//
//  Created by Lucas Gomes on 16/05/23.
//

import SwiftUI
import Combine

class UserStore: ObservableObject {
    @Published var isLogged: Bool = UserDefaults.standard.bool(forKey: "isLogged") {
        didSet {
            UserDefaults.standard.set(isLogged, forKey: "isLogged")
        }
    } // Show/Hide Avatar and menu
    @Published var showLogin = false // Show/Hide Login screen
}
