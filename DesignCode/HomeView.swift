//
//  HomeView.swift
//  DesignCode
//
//  Created by Lucas Gomes on 10/05/23.
//

import SwiftUI

struct HomeView: View {
    @Binding var showProfile: Bool
    @Binding var showContent: Bool
    @State var showUpdate = false
    @Binding var viewState: CGSize
    @ObservedObject var store = CourseStore()
    @State var active = false
    @State var activeIndex = -1
    @State var activeView = CGSize.zero
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    @State var isScrollable = false
    
    var body: some View {
        GeometryReader { bounds in
            ScrollView {
                VStack {
                    HStack {
                        Text("Watching")
                            .modifier(CustomFontModifier(size: 28))
        //                    .font(.system(size: 28, weight: .bold))
                        
                        Spacer()
                        
                        AvatarView(showProfile: $showProfile)
                        
                        //MARK: - Modal apresentation
                        Button {
                            showUpdate.toggle()
                        } label: {
                            Image(systemName: "bell")
                                .tint(.primary)
                                .font(.system(size: 16, weight: .medium))
                                .frame(width: 36, height: 36)
                                .background(Color("background3"))
                                .clipShape(Circle())
                                .shadow(color: .black.opacity(0.1), radius: 1, x: 0, y: 1)
                                .shadow(color: .black.opacity(0.2), radius: 10, x: 0, y: 10)
                        }
                        .sheet(isPresented: $showUpdate) {//MODAL APRESENTATION
                            UpdateList()
                        }
                        //MARK: - End (Modal apresentation)
                    }
                    .padding(.horizontal)
                    .padding(.top, 30)
                    .padding(.leading, 14)
                    .blur(radius: active ? 20 : 0)
                    
                    ScrollView(.horizontal, showsIndicators: false) {
                        WatchRingsView()
                            .padding(.horizontal, 30)
                            .padding(.bottom, 40)
                            .onTapGesture {
                                showContent = true
                            }
                    }
                    .blur(radius: active ? 20 : 0)
                    
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack(spacing: 20) {
                            ForEach(sectionData) { item in
                                GeometryReader { geometry in
                                    SectionView(section: item)
                                        .rotation3DEffect(
                                            .degrees(
                                                Double(geometry.frame(in: .global).minX - 30) / -getAngleMultiplier(bounds)
                                            ),
                                            axis: (x: 0, y: 10, z: 0))
                                }
                                .frame(width: 275, height: 275)
                            }
                        }
                        .padding(30)
                        .padding(.bottom, 30)
                    }
                    .offset(y: -40)
                    .blur(radius: active ? 20 : 0)
                    
                    HStack {
                        Text("Courses")
                            .font(.title)
                            .bold()
                        Spacer()
                    }
                    .offset(y: -60)
                    .padding(.leading, 30)
                    .blur(radius: active ? 20 : 0)
                    
//                    SectionView(width: bounds.size.width - 60, height: 275, section: sectionData[2])
//                        .offset(y: -60)
                    
          
                    VStack(spacing: 30) {
                        ForEach(store.courses.indices, id: \.self) { index in
                            GeometryReader { geometry in
                                CourseView(
                                    show: $store.courses[index].show,
                                    course: store.courses[index],
                                    active: $active,
                                    index: index,
                                    activeIndex: $activeIndex,
                                    activeView: $activeView,
                                    bounds: bounds,
                                    isScrollable: $isScrollable
                                )
                                .offset(y: store.courses[index].show ? -geometry.frame(in: .global).minY : 0)//Joga para o topo
                                .opacity(active && activeIndex != index ? 0 : 1)//Vai nos cards restantes que nao estao selecionados
                                .scaleEffect(active && activeIndex != index ? 0.5 : 1)//Vai nos cards restantes que nao estao selecionados
                                .offset(x: active && activeIndex != index ?  bounds.size.width : 0)//Vai nos cards restantes que nao estao selecionados
                            }
                            .frame(height: horizontalSizeClass == .regular ? 80 : 280)
                            .frame(maxWidth: store.courses[index].show ? 712 : getCardWidth(bounds))//MARK: 712 tamanho maximo recomendado para ipad
                            .zIndex(store.courses[index].show ? 1 : 0)
                        }
                    }
                    .padding(.bottom, 300)
                    .offset(y: -60)
                    
                    
                    Spacer()
                }
                .frame(width: bounds.size.width)//Ajusta sempre que mudar a largura
                .clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
                .shadow(color: .black.opacity(0.2), radius: 20, x: 0, y: 20)
                .offset(y: showProfile ? -450 : 0)
                .rotation3DEffect(.degrees(showProfile ? Double(viewState.height / 12) - 10 : 0), axis: (x: 10, y: 0, z: 0))
                .scaleEffect(showProfile ? 0.9 : 1)
                .animation(.spring(response: 0.5, dampingFraction: 0.6), value: showProfile)
                .animation(.spring(response: 0.2, dampingFraction: 0.3), value: viewState)
            }
//            .scrollDisabled(active ? true : false)
//            .statusBarHidden(active ? true : false)
        }
    }
}

func getAngleMultiplier(_ bounds: GeometryProxy) -> Double {
    if bounds.size.width > 500 {
        return 80
    } else {
        return 20
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(showProfile: .constant(false), showContent: .constant(false), viewState: .constant(.zero))
            .environmentObject(UserStore())
    }
}

struct SectionView: View {
    var width: CGFloat = 275
    var height: CGFloat = 275
    var section: Section
    
    var body: some View {
        VStack {
            HStack(alignment: .top) {
                Text(section.title)
                    .font(.system(size: 24, weight: .bold))
                    .frame(width: 160, alignment: .leading)
                    .foregroundColor(.white)
                Spacer()
                Image(section.logo)
            }
            
            Text(section.text.uppercased())
                .frame(maxWidth: .infinity, alignment: .leading)
            
            section.image
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 210)
        }
        .padding(.top, 20)
        .padding(.horizontal, 20)
        .frame(width: width, height: height)
        .background(section.color)
        .cornerRadius(30)
        .shadow(color: section.color.opacity(0.3), radius: 20, x: 0, y: 20)
    }
}

struct Section: Identifiable {
    let id = UUID()
    var title: String
    var text: String
    var logo: String
    var image: Image
    var color: Color
}

let sectionData = [
    Section(title: "Prototype design in SwiftUI", text: "18 Sections", logo: "Logo1", image: Image(uiImage: #imageLiteral(resourceName: "Card4")), color: Color(#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1))),
    Section(title: "Build SwiftUI App", text: "20 Sections", logo: "Logo1", image: Image(uiImage: #imageLiteral(resourceName: "Card5")), color: Color(#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1))),
    Section(title: "SwiftUI Advance", text: "18 Sections", logo: "Logo1", image: Image(uiImage: #imageLiteral(resourceName: "Card1")), color: Color(#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))),
]

struct WatchRingsView: View {
    var body: some View {
        let ring1Color1 = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
        let ring1Color2 = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        let ring2Color1 = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        let ring2Color2 = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        let ring3Color1 = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        let ring3Color2 = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        
        HStack(spacing: 30.0) {
            HStack(spacing: 12.0) {
                RingView(color1: ring1Color1, color2: ring1Color2, width: 44, height: 44, percent: 68, show: .constant(true))
                
                VStack(alignment: .leading, spacing: 4.0) {
                    Text("6 minutes left")
                        .bold()
                        .modifier(FontModifier(style: .subheadline))
                    //                        .font(.subheadline)
                    //                        .fontWeight(.bold)
                    
                    Text("Watched 10 minutes today")
                        .modifier(FontModifier(style: .caption))
                    //                        .font(.caption)
                }
            }
            .padding(8)
            .background(Color("background3"))
            .cornerRadius(20)
            .modifier(ShadowModifier())
            
            HStack(spacing: 12.0) {
                RingView(color1: ring2Color1, color2: ring2Color2, width: 32, height: 32, percent: 54, show: .constant(true))
            }
            .padding(8)
            .background(Color("background3"))
            .cornerRadius(20)
            .modifier(ShadowModifier())
            
            HStack(spacing: 12.0) {
                RingView(color1: ring3Color1, color2: ring3Color2, width: 32, height: 32, percent: 83, show: .constant(true))
            }
            .padding(8)
            .background(Color("background3"))
            .cornerRadius(20)
            .modifier(ShadowModifier())
        }
    }
}
