//
//  Card3dTesting.swift
//  DesignCode
//
//  Created by Lucas Gomes on 13/05/23.
//

import SwiftUI

struct Card3dTesting: View {
    @State private var rotationAngleX: Double = 0.0
    @State private var rotationAngleY: Double = 0.0
    
    var body: some View {
        VStack {
            Text("Deslize o dedo para inclinar")
                .font(.headline)
                .padding()
            
            RoundedRectangle(cornerRadius: 10)
                .foregroundColor(.blue)
                .frame(width: 300, height: 300)
                .padding()
                .rotation3DEffect(
                    Angle(degrees: rotationAngleX),
                    axis: (x: 1.0, y: 0.0, z: 0.0)
                )
                .rotation3DEffect(
                    Angle(degrees: rotationAngleY),
                    axis: (x: 0.0, y: 1.0, z: 0.0)
                )
                .gesture(
                    DragGesture()
                        .onChanged { value in
                            let vector = CGVector(dx: value.translation.width, dy: value.translation.height)
                            rotationAngleX = Double(-vector.dy / 30)// 100 -> less angle
                            rotationAngleY = Double(vector.dx / 30)// (-vector.dx) & (vector.dy) to invert rotation
                        }
                        .onEnded { _ in
                            rotationAngleX = 0.0
                            rotationAngleY = 0.0
                        }
                )
        }
    }
}

struct CardTesting_Previews: PreviewProvider {
    static var previews: some View {
        Card3dTesting()
    }
}
