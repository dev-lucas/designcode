//
//  TabBar.swift
//  DesignCode
//
//  Created by Lucas Gomes on 10/05/23.
//

import SwiftUI

struct TabBar: View {
    let color = UIColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 0.1463938328))

    init() {
        UITabBar.appearance().backgroundColor = UIColor.secondarySystemBackground
        UITabBar.appearance().isTranslucent = true
        UIButton.appearance().accessibilityPath?.lineWidth = 2
    }

    var body: some View {
        TabView {

            Home()
                .tabItem {
                    Image(systemName: "play.circle.fill")
                    Text("Home")
                }
            CourseList()
                .tabItem {
                    Image(systemName: "rectangle.stack.fill")
                    Text("Courses")
                }
        }
//        .ignoresSafeArea()

        

    }
}

struct TabBar_Previews: PreviewProvider {
    static var previews: some View {
        TabBar()
            .environmentObject(UserStore())
    }
}
