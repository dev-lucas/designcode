//
//  DataStore.swift
//  DesignCode
//
//  Created by Lucas Gomes on 12/05/23.
//

import SwiftUI
import Combine

class DataStore: ObservableObject {
    @Published var posts: [Post] = []
    
    init() {
        getPosts()
    }
    
    func getPosts() {
        Api().getPosts { [weak self] posts in
            self?.posts = posts
        }
    }
}
