//
//  Data.swift
//  DesignCode
//
//  Created by Lucas Gomes on 11/05/23.
//

import SwiftUI

struct Post: Codable, Identifiable {
    let id = UUID()//Keep let for Identifiable
    var title: String
    var body: String
}

class Api {
    func getPosts(completion: @escaping ([Post]) -> Void) {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts") else { return completion([]) }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data else { return completion([]) }
            guard let posts = try? JSONDecoder().decode([Post].self, from: data) else { return completion([]) }

            DispatchQueue.main.async {
                completion(posts)
            }
        }
        .resume()
    }
}
