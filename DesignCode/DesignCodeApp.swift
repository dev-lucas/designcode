//
//  DesignCodeApp.swift
//  DesignCode
//
//  Created by Lucas Gomes on 09/05/23.
//

import SwiftUI
import FirebaseCore

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        FirebaseApp.configure()
        return true
    }
}

@main
struct DesignCodeApp: App {
    // register app delegate for Firebase setup
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    var body: some Scene {
        WindowGroup {
            NavigationView {
                Home()
            }
            .environmentObject(UserStore())
            .navigationViewStyle(StackNavigationViewStyle())
        }
    }
}
