//
//  CourseStore.swift
//  DesignCode
//
//  Created by Lucas Gomes on 12/05/23.
//

import SwiftUI
import Contentful
import Combine

/*
 Space ID
 y86v4c4peepp
 
 Content Delivery API - access token
 5BVOQ2ub4oKmF1aZ_FxgPFw4eMaiGphxcMbtfOpxGEs
 
 Content Preview API - access token
 8uK_dunghkSsGgt_9F88SY0oUJl1yQTpb8fcwtyeh_w
 
 */

let client = Client(spaceId: "y86v4c4peepp", accessToken: "5BVOQ2ub4oKmF1aZ_FxgPFw4eMaiGphxcMbtfOpxGEs")

func getArray(id: String, completion: @escaping([Entry]) -> Void) {
    let query = Query.where(contentTypeId: id)
    client.fetchArray(of: Entry.self, matching: query) { result in
        switch result {
        case .success(let entry):
            DispatchQueue.main.async {
                completion(entry.items)
            }
        case .failure(let error):
            print("Error \(error)!")
        }
    }
}

class CourseStore: ObservableObject {
    @Published var courses: [Course] = courseData
    
    init() {
        let colors = [#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1), #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1), #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1), #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)]
        var index = 0
        
        getArray(id: "course") { [weak self] items in
            items.forEach { item in
                self?.courses.append(
                    Course(title: item.fields["title"] as? String ?? "-",
                           subtitle: item.fields["subtitle"] as? String ?? "-",
                           image: item.fields.linkedAsset(at: "image")?.url ?? URL(string: "")!,
                           logo: UIImage(imageLiteralResourceName: "Logo1"),
                           color: colors[index],
                           show: false)
                )
                
                index += 1
            }
        }
    }
}
