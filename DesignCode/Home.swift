//
//  Home.swift
//  DesignCode
//
//  Created by Lucas Gomes on 10/05/23.
//

import SwiftUI

struct Home: View {
    @State var showProfile = false
    @State var viewState = CGSize.zero
    @State var showContent = false
    @EnvironmentObject var user: UserStore
    
    var body: some View {
        ZStack {
            Color("background2")
                .ignoresSafeArea()
            
            HomeBackgroundView(showProfile: $showProfile)
                .offset(y: showProfile ? -450 : 0)
                .rotation3DEffect(.degrees(showProfile ? Double(viewState.height / 12) - 10 : 0), axis: (x: 10, y: 0, z: 0))
                .scaleEffect(showProfile ? 0.9 : 1)
                .animation(.spring(response: 0.5, dampingFraction: 0.6), value: showProfile)
                .animation(.spring(response: 0.2, dampingFraction: 0.3), value: viewState)
                .ignoresSafeArea()
            
//            TabView {
                HomeView(showProfile: $showProfile, showContent: $showContent, viewState: $viewState)
//                    .scrollDisabled(showContent ? true : false)
//                    .tabItem {
//                        Image(systemName: "play.circle.fill")
//                        Text("home")
//                    }
//            }
//                .padding(.top, 44)
//                .background(
//                    VStack {
//                        LinearGradient(colors: [Color("background2"), Color("background1")], startPoint: .top, endPoint: .bottom)
//                            .frame(height: 200)
//                        Spacer()
//                    }
//                        .background(Color("background1"))
//                )
//                .clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
//                .shadow(color: .black.opacity(0.2), radius: 20, x: 0, y: 20)
//                .offset(y: showProfile ? -450 : 0)
//                .rotation3DEffect(.degrees(showProfile ? Double(viewState.height / 12) - 10 : 0), axis: (x: 10, y: 0, z: 0))
//                .scaleEffect(showProfile ? 0.9 : 1)
//                .animation(.spring(response: 0.5, dampingFraction: 0.6), value: showProfile)
//                .animation(.spring(response: 0.2, dampingFraction: 0.3), value: viewState)
//                .ignoresSafeArea()
//                .onTapGesture {
//                    showProfile = false
//                }
            
            //Text("Value: \(viewState.height)").offset(y: -300)
            
            MenuView(showProfile: $showProfile)
                .background(Color.black.opacity(0.001))
                .offset(y: showProfile ? 0 : screen.height)
                .offset(y: viewState.height/2)
                .animation(.spring(response: 0.5, dampingFraction: 0.6), value: showProfile)
                .animation(.spring(response: 0.2, dampingFraction: 0.5), value: viewState)
                .gesture(
                    DragGesture()
                        .onChanged { value in
                            viewState = value.translation
                        }
                        .onEnded { value in
                            //Fecha o menu quando puxar para baixo
                            if viewState.height > 50 {
                                showProfile = false
                            }
                            viewState = .zero
                        }
                )
            
            if user.showLogin {
                ZStack {
                    LoginView()
                    
                    VStack {
                        HStack {
                            Spacer()
                            Image(systemName: "xmark")
                                .frame(width: 36, height: 36)
                                .foregroundColor(.white)
                                .background(Color.black)
                                .clipShape(Circle())
                        }
                        Spacer()
                    }
                    .padding()
                    .onTapGesture {
                        user.showLogin.toggle()
                    }
                }
            }
            
            if showContent {//Melhor para performance
                BlurView().ignoresSafeArea()
                
                ContentView()
                
                VStack {
                    HStack {
                        Spacer()
                        Image(systemName: "xmark")
                            .frame(width: 36, height: 36)
                            .foregroundColor(.white)
                            .background(Color.black)
                            .clipShape(Circle())
                    }
                    Spacer()
                }
                .offset(x: -16, y: 16)
                .transition(.move(edge: .top))
                .animation(.spring(response: 0.6, dampingFraction: 0.8), value: showContent)
                .onTapGesture {
                    showContent = false
                }
            }
        }
//        .scrollDisabled(showProfile ? true : false)
    }
}

struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Home()
            .environmentObject(UserStore())
//            .environment(\.colorScheme, .dark)
//            .environment(\.sizeCategory, .extraExtraLarge)
    }
}

struct AvatarView: View {
    @Binding var showProfile: Bool
    @EnvironmentObject var user: UserStore
    
    var body: some View {
        VStack {
            if !user.isLogged {
                Button {
                    showProfile.toggle()
                } label: {
                    Image("Avatar")
                        .resizable()
                        .frame(width: 36, height: 36)
                        .clipShape(Circle())
                }
            } else {
                Button {
                    user.showLogin.toggle()
                } label: {
                    Image(systemName: "person")
                        .tint(.primary)
                        .font(.system(size: 16, weight: .medium))
                        .frame(width: 36, height: 36)
                        .background(Color("background3"))
                        .clipShape(Circle())
                        .shadow(color: .black.opacity(0.1), radius: 1, x: 0, y: 1)
                        .shadow(color: .black.opacity(0.2), radius: 10, x: 0, y: 10)
                }
            }
        }
    }
}

let screen = UIScreen.main.bounds

struct HomeBackgroundView: View {
    @Binding var showProfile: Bool
    var body: some View {
        VStack {
            LinearGradient(colors: [Color("background2"), Color("background1")], startPoint: .top, endPoint: .bottom)
                .frame(height: 200)
            Spacer()
        }
        .background(Color("background1"))
        .clipShape(RoundedRectangle(cornerRadius: showProfile ? 30 : 0, style: .continuous))
        .shadow(color: .black.opacity(0.2), radius: 20, x: 0, y: 20)
    }
}
