//
//  UpdateStore.swift
//  DesignCode
//
//  Created by Lucas Gomes on 10/05/23.
//

import SwiftUI
import Combine

class UpdateStore: ObservableObject {
    //@Published -> Atualiza os dados em tempo real
    @Published var updates: [Update] = updateData
    
}
